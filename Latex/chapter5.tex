\chapter{\sc Question Classification}
In this chapter we are going to analyse some of the available classification algorithms, the training set that was used, the test they underwent that brought us to choose a particular algorithm and its performances.  
\section{\sc Classification Algorithms} 
As we said before we need to train an artificial intelligence to classify a question in one of seven different classes, based on the type of information asked, between \textbf{Def, When, Creator, Commissioner, Renovation, Style} and \textbf{Measurement}. There are a lot of classification algorithms, mostly based on a supervised learning approach. This means that they need a group of already tagged data to infer the correlations between data belonging to the same group, to subsequently put a new piece of data in the correct group. Let us have a brief overlook to some of the classification algorithms. Being them classification algorithms they have all the same purpose, even if attained in different ways: given a set of \emph{n} classes $ C = {c_1, c_2, ... c_n} $ and a piece of data $X$ made up of a set of \emph{m} attributes such as $X = {x_1, x_2, ... x_m}$, our interest is to calculate, for each class $c_i$, the probability that $X$ belongs to $c_i$, $P(X \in c_i)$, so to assign $X$ to the class with the higher probability.

\subsection{Naive Bayes Classifier}
This classifier is based on the Bayes' Theorem and assumes that the effect of an attribute value on a given class is independent of the values of the other attributes. This assumption is called \textbf{class conditional independence}. It is typically not true, but it is made to simplify the computation involved. Thus this kind of Classifier is considered (and called) "naive".\\
Let us consider our piece of data $X={x_1,x_2, ... , x_n}$ as defined before. In Bayesian terms, $X$ is considered "evidence". Let $H$ be some hypothesis, such as that the data $X$ belongs to a specific class $C$. $P(H\lvert X)$ is the a posteriori probability of $H$ conditioned on $X$. For example, suppose our data samples have just one attribute, the age, and that sample $X$ is a 35-year-old person. Suppose that $H$ is the hypothesis that our person is a drug user. Then $P(H\lvert X)$ is the probability that person $X$ is a drug user given that we know the person's age. In contrast, $P(H)$ is the a priori probability of $H$. For our example, this is the probability that any given person will buy a drug user, regardless of age or any other information. The a posteriori probability $P(H\lvert X)$ is based on more information (about the person) than the a priori probability, $P(H)$, which is independent of $X$. Similarly, $P(X\lvert H)$ is the a posteriori probability of $X$ conditioned on $H$. That is, it is the probability that a given person $X$, is 35 years old, given that we know that he is a drug user. $P(X)$ is the a priori probability of $X$. In our example, it is the probability that a person from our set of drug users is 35 years old. 
\begin{theorem}
The probability $P(H\lvert X)$ can be expressed in terms of probabilities $P(H)$,$P(X\lvert H)$, and $P(X)$ as $P(H\lvert X) = \frac{P(X\lvert H)P(H)}{P(X)}$.
\end{theorem}
This theorem is particularly useful in our problem, since $P(X\lvert H)$ is usually easier to calculate with respect to $P(H\lvert X)$ (as it is usually obtainable from historical data).\\
Given our typical classification problem, with $k$ classes $C_1,C_2, ... , C_k$, we are going to calculate, $\forall i \in [1,k]$, $P(C_i\lvert X)$ and \textbf{predict} that our data $X$ belongs to $C_i$ class if and only if $P(C_i\lvert X) > P(C_j \lvert X)$ for $1<=j<=k, j \neq i$ and call that the \textbf{maximum posteriori hypothesis}.\\
Basically we are aiming to maximize $P(C_i\lvert X)$ over $i$ or, given Bayes' Theorem, $\frac{P(X\lvert C_i)P(C_i)}{P(X)}$. Since $P(X)$ is constant over $i$, we are going to maximize $P(X\lvert C_i)P(C_i)$, where $P(C_i)$ can be estimated from historical data.\\
If $X$ were to be composed of a large number of attributes, the calculation of $P(X\lvert C_i)$ would result very complicated, since we would have to keep in mind the covariation matrix. Here come the "naive" part of the classifier, where we assume that the class conditional independence hold. Basically this means that $P(X\lvert C_i)$ can be approximated to $\prod_{l=1}^n P(x_l\lvert C_i)$. Again, this simplify our work a lot, since even  $P(x_l\lvert C_i)$ can be computed easily from historical data.
\cite{naivebayes}

\subsection{Logistic Regression} 
The logistic regression algorithm is based on the so called \textbf{sigmoid function}, also called logistic function for its use in this algorithm, an S-shaped function that can take any real valued number and map it into the interval $(0,1)$. This function was developed by statisticians to describe properties of population growth in ecology, rising quickly and maxing out at the carrying capacity of the environment. We can see it in Figure \ref{fig:sigmoid}.
\begin{figure}[ht]
\centering
\includegraphics[scale=0.5]{Immagini/sigmoid.png}
\caption{\textit{Sigmoid Function}}
\label{fig:sigmoid}
\end{figure}
The most general version of the sigmoid function is $y = \frac{\exp(b_0 + \beta^T X)}{1+\exp(b_0 + \beta^T X)} $ where $b_0$ is the intercept and $\beta$ is a vector that groups the regression coefficients. This expression can be easily transformed and exploited to show a linear correlation between the so called \textbf{logit function} $\ln(\frac{y}{1-y})$ and the superscript $b_0 + \beta^T X$.
Indeed from $y = \frac{\exp(b_0 + \beta^T X)}{1+\exp(b_0 + \beta^T X)} $ follows:\\
\begin{equation}
\begin{split}
&y + y\exp(b_0 + \beta^T X) = \exp(b_0 + \beta^T X) \Rightarrow  \\
&y = (1-y)\exp(b_0 + \beta^T X) \Rightarrow \\
&\frac{y}{1-y}=\exp(b_0 + \beta^T X) \Rightarrow \\ 
&\ln(\frac{y}{1-y})= b_0 + \beta^T X
\end{split}
\end{equation}
Proven that,with the same terminology as before, let us define $y_i$ as $y_i=P(X \in C_i \lvert X) $. At this point the logit function $\ln(\frac{y_i}{1-y_i})$ will be the natural logarithm of the odds of $X$ belonging to $C_i$. At this point we just need to find the regression coefficients, that are usually estimated trough Maximum Likelihood.\cite{logisticregression}

\subsection{Linear Support Vector Classifier}
As in every other classification algorithm the objective of Linear SVC is to fit to the data provided (our knowledge base dataset), returning a "best fit" hyperplane that divides, and thus categorizes, the given data. After getting the hyperplane, it is possible to feed other piece of data to the classifier, which will assign the data to the class corresponding to the hyperspace where it resides.\\
What we said is easily seen in a space with two features, since every feature is going to lay on an axis. Refer to Fig. \ref{fig:svc1}; the coloured points represents piece of data previously classified to train our algorithm. After the training the Linear SVC will provide a line that will divide the plane in two demiplanes. If a new piece of data is going to be classified the algorithm will simply position it in the space and calculate its belonging demiplane.   
\begin{figure}
        \centering
        \begin{subfigure}[b]{0.35\textwidth}
    		\centering
        	\includegraphics[width=\textwidth]{Immagini/linearsvc1.png}
        	\caption[3550]%
        	{{\small \textit{Data in 2D feature space}}}
        	\label{fig:svc1}  
    	\end{subfigure}
    	\hfill
    	\begin{subfigure}[b]{0.35\textwidth}  
        	\centering 
        	\includegraphics[width=\textwidth]{Immagini/linearsvc2.png}
        	\caption[]%
        	{{\small \textit{Linear SVC separates data}}}
       	 	\label{fig:svc2}    
    	\end{subfigure}
        \caption{ } 
        \label{fig:svc}
\end{figure}
Of course this process can be generalized to \emph{m} features and \emph{n} classes, thus returning to our base case.

\section{\sc Benchmarking Classification Algorithms}
To benchmark the four classification algorithm, available trough the scikit\_learn Python package, we created a list of correct question-tag tuples. We evaluated the \textbf{accuracy} of each algorithm and then tested the best of them in various conditions, using different chunks of the training set, from a 25 percent trough a 90 percent, to evaluate how well the algorithm would have acted on small datasets. As metrics, aside from the confusion matrix, that gives a visual hint on the performances, we calculated the most common metrics: \textbf{precision}, \textbf{recall} and the \textbf{F measure}.
\\
Let us give a brief description of the above metrics. It is important to note that every classification problem, even with more than two classes as in our case, can be rewritten as \textit{k} binary problems, one for each of the \textit{k} classes, where the question is now "\textit{Does this item belong to this class?}" and the available answers are \textit{Yes} and \textit{No}. That said we can explain the various metrics on a binary problem without any loss of generality.\\
For a binary problem we can have for different results for any generic prediction:
\begin{enumerate}
\item \textbf{True Positive}, when the answer should be \textit{Yes} and it is, actually, \textit{Yes};
\item \textbf{True Negative}, when the answer should be \textit{No} and it is, actually, \textit{No};
\item \textbf{False Positive}, when the answer should be \textit{No} and instead it is \textit{Yes};
\item \textbf{False Negative}, when the answer should be \textit{Yes} and instead it is \textit{No}.
\end{enumerate}
In this situation we can easily define our metrics.
\paragraph{Accuracy} is the number of the correct positive predictions (the True Positives) over the total number of predictions. 
\begin{equation}
A = \frac{P_T}{P_T+P_F+N_T+N_F}
\end{equation}
Accuracy as a performance metrics can be misleading. This is referred as the \textbf{Accuracy Paradox}\cite{accparadox}, and it states that in some cases an algorithm with a lower accuracy can actually be a best fit to a problem than an algorithm with an higher accuracy. Example given, in cancer detection we may care a lot about the False Negatives (since this would mean not giving proper medical assistance to someone that could survive was it caught in time), but accuracy does not give us any information about them.
\paragraph{Precision} is the number of True Positives divided by the number of True Positives and False Positives. It is also called the Positive Predictive Value (PPV).
\begin{equation}
PPV = \frac{P_T}{P_T+P_F}
\end{equation}
Precision can be thought of as a measure of a classifiers exactness. A low precision can also indicate a large number of False Positives.

\paragraph{Recall} is the number of True Positives divided by the sum of True Positives and False Negatives; basically it is the number of positive predictions divided by the number of positive class values in the test data.
\begin{equation}
R = \frac{P_T}{P_T+N_F}
\end{equation}
Recall can be thought of as a measure of a classifiers completeness. A low recall indicates many False Negatives. In our cancer detection scenario, for example, we would prefer an algorithm with a very high recall.

\paragraph{F Measure} is an estimator that combine the information given by Precision and Recall.
\begin{equation}
F = 2 \cdot \frac{PPV \cdot R}{PPV+R}
\end{equation}

\subsection{Training and test data}
As said before, our training and test set is basically a list couples of question and correct tag. We are going to give a brief example, but the entire dataset can be found in the Appendix. 

\begin{verbatim}
Quanto è alto?,Measurement
In che stile è costruito Palazzo del Seggio?,Style
Quando è stato costruito Palazzo Carafa?,When
Cos'è il Mosaico della Lupa?,Def
Chi è l’architetto di Palazzo del Seggio?,Creator
Chi ha commissionato Palazzo Carafa?,Commissioner
Quando è stato ristrutturato Mosaico della Lupa?,Renovation
\end{verbatim}  

Our dataset is composed of 138 couples and, more precisely:
\begin{itemize}
\item 8 couples relative to the \textbf{Measurement} tag;
\item 14 couples relative to the \textbf{Style} tag;
\item 33 couples relative to the \textbf{When} tag;
\item 21 couples relative to the \textbf{Def} tag;
\item 17 couples relative to the \textbf{Creator} tag;
\item 29 couples relative to the \textbf{Commissioner} tag;
\item 16 couples relative to the \textbf{Renovation} tag;
\end{itemize}

The uneven distribution of the questions is mostly random, but it can be used to infer how (and if) a small group of data regarding a class can mess with the classification algorithm.
\subsection{Results of the benchmarking}
We said before that accuracy is not a good estimator of the quality of an algorithm on a given problem. That said, it can be used anyway to get an idea of how good an algorithm is generally speaking, since it follows easily that if the measured accuracy is really high, around say 80 percent, the other measures will be good too. In Figure \ref{fig:models_accuracy} we can see the accuracies resulted from our testing on the considered models, while in Figure \ref{fig:stat35} are reported accuracy, recall and F measure of those models, calculated using 65\% of the dataset as training data and the remaining 35\% as testing data. In Figure \ref{fig:matrix35} we can see the confusion matrices relative to these tests.
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{Immagini/ModelStats/models_accuracy.png}
\caption{\textit{Models Accuracy}}
\label{fig:models_accuracy}
\end{figure}

\begin{figure*}
        \centering
        \begin{subfigure}[b]{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LinearSVC/stat35.png}
            \caption[2350]%
            {{\small Linear SVC results}}    
            \label{fig:statLinear35}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{Immagini/ModelStats/MultinomialNB/stat35.png}
            \caption[2350]%
            {{\small Multinomial NB results}}    
            \label{fig:statNB35}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LogisticRegression/stat35.png}
            \caption[2350]%
            {{\small Logistic Regression results}}    
            \label{fig:statRegression}
        \end{subfigure}
        \caption{\small Accuracy, Recall and F measure of the three models} 
        \label{fig:stat35}
\end{figure*}
\begin{figure*}
        \centering
        \begin{subfigure}[b]{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LinearSVC/matrix35.png}
            \caption[2350]%
            {{\small Linear SVC}}    
            \label{fig:matrixLinear35}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{Immagini/ModelStats/MultinomialNB/matrix35.png}
            \caption[2350]%
            {{\small Multinomial NB}}    
            \label{fig:matrixNB35}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LogisticRegression/matrix35.png}
            \caption[2350]%
            {{\small Logistic Regression}}    
            \label{fig:matrixRegression35}
        \end{subfigure}
        \caption{\small Confusion Matrix of the results of the three models} 
        \label{fig:matrix35}
\end{figure*}
As we can see the performances of the \textbf{LinearSVC} classifier skyrockets to an almost impossible \%100 accuracy and recall, which points us to probably the best classifier for the problem at hand. The other classifiers, even with the large chunk of dataset provided as training set, did not give comparable results, even if they could have been deemed adequate in a different situation.\\  

\subsection{Performances of the Linear SVC}
At this point, once we figured out the best classifier out of our three candidates, we started training the LinearSVC classifier on different chunks of the training set and testing it on the residual part, trying to strain the algorithm while we reduced the dimension of training set more and more, to see how badly the performances would have dropped. In Figure \ref{fig:matrices} we can look at the confusion matrices produced by the algorithm; as we can see the, while the dimension of training set keeps reducing, from the 65\% of the first test in Figure \ref{fig:SVCmatrix35}, to the risible 15\% of the last test in Figure \ref{fig:SVCmatrix85}, the performances, while lowering as could be expected, never drops below the performances seen for Multinomial NB classifier (Figure \ref{fig:matrixNB35}) or the Linear Regression classifier (Figure \ref{fig:statLinear35}) that, we should remember, where calculated with a training set at the 65\% of our dataset. We can check this data in Figure \ref{fig:statlinear85}.
\begin{figure*}
        \centering
        \begin{subfigure}[b]{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LinearSVC/matrix35.png}
            \caption[3550]%
            {{\small Training set size: 65\%}}    
            \label{fig:SVCmatrix35}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.475\textwidth}  
            \centering 
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LinearSVC/matrix50.png}
            \caption[]%
            {{\small Training set size: 50\%}}    
            \label{fig:SVCmatrix50}
        \end{subfigure}
        \vskip\baselineskip
        \begin{subfigure}[b]{0.475\textwidth}   
            \centering 
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LinearSVC/matrix75.png}
            \caption[]%
            {{\small Training set size: 25\%}}    
            \label{fig:SVCmatrix75}
        \end{subfigure}
        \quad
        \begin{subfigure}[b]{0.475\textwidth}   
            \centering 
            \includegraphics[width=\textwidth]{Immagini/ModelStats/LinearSVC/matrix85.png}
            \caption[]%
            {{\small Training set size: 15\%}}    
            \label{fig:SVCmatrix85}
        \end{subfigure}
        \caption[]
        {\small Confusion Matrices of the straining test} 
        \label{fig:matrices}
\end{figure*}

\begin{figure}
\centering
\includegraphics[scale=0.8]{Immagini/ModelStats/LinearSVC/stat35.png}
\caption{\small Performances of the LinearSVC for a training set size at 15\% }
\label{fig:statlinear85}
\end{figure}