\chapter{\sc Project Analysis}

\section{\sc Project Requirements}

This project is intended to be a question answering system that will try to enhance the tourist experience, providing a guide that can answer to a number of questions about monuments that the user may see and be interested in. For simplicity's sake, it will be focused on Sant'Oronzo'square in Lecce. 
The project is going to be composed of two different software artifacts: 
\begin{itemize}
\item A Python server, which is going to provide the backend work of question analysis and answer retrieval;
\item An hybrid mobile application, developed with Ionic3, which is going to provide the user interface.
\end{itemize}  

In the previous chapter we analysed the reasons behind our decision regarding the technology to be used for the artefact that will be presented to the user. For the backend artefact we have chosen to use Python because this programming language has a number of built in function that can deal with natural language processing and machine learning, thus sparing us the necessity to dabble with external plugins.

\subsection{The expected user experience}
The user is expected to interact with the mobile app in Sant'Oronzo'Square, and a working GPS positioning is required. While walking or stationing in the said square the user will be prompted with an image and a brief description of the nearest monument or point of interest from our selection, that follows:
\begin{itemize}
\item \textbf{Chiesa di Santa Maria della Grazia}, a baroque church;
\item \textbf{Anfiteatro Romano}, the most important expression of the antique importance of Lupiae, the Roman name for Lecce;
\item \textbf{Mosaico della Lupa}, a mosaic that ties the modern Lecce to Lupiae;
\item \textbf{Palazzo del Seggio}, an historic building built from the venetian community in the XVI century;
\item \textbf{Colonna di Sant'Oronzo}, one of the more known symbol of the Square;
\item \textbf{Chiesetta di San Marco}, a church built in the same period of Palazzo del Seggio, under the direction of the venetian community;
\item \textbf{Palazzo Carafa}, an historic building now serving as town hall;
\item \textbf{Palazzo di Giustizia}, formerly a Jesuit monastery built in the XVI century;
\end{itemize}

At the same time the user can pose a question about the showed monument. It is crucial for the question asked to be of the factoid kind, but, given the boundaries of the area of interest (the monuments of the Square) that doesn't seems really limiting. The application is required to answer the given question with a short phrase. 

\section{\sc The Python Server: first attempt}
Here we are going to show our first attempt to develop the server application and explain how the server is going process the data, how the algorithm will be trained and in which way it will answer the questions posed. We will see later that this approach did not quite work out as we hoped, so we had to change our project on the fly.

\subsection{Passages and Questions}
Our server is equipped with a dataset in JSON which brings information on every monument of our selection in the following form:
\begin{lstlisting}[language=json,firstnumber=1]
{
  "monuments": [
    {
  	  "ID" : 1,
	  "Name": "Chiesa di Santa Maria della Grazia",
	  "Latitude": "40.352420",
	  "Longitude": "18.173369",
	  "ShortDesc": "Some Description, let s keep it short",	
	  "LongDesc": "A couple of phrases, maybe more,
	  describing the monument. <break>
	  The description is broken into passages 
	  with this tags <break>",	
	  "Url": "http://www.someurl",
	  "Picture": "http://www.someurl.jpg"
	}
  ]
}
\end{lstlisting}

In the following text, when we talk about a \textbf{passage}, we will mean a unit of the "Long Description", as separated trough \textbf{<break>} tags. 
Questions will be posed from the user, though of course we have a dataset of questions, useful for the training of the algorithm in certain ways.

It is important to note that passages and questions are almost treated in the same way, meaning that the algorithm distinguish them rarely. 

\subsection{The Preprocessing} 
As we said both passages and question undergo the same treatment, which is mainly composed by a pipeline of actions. Let us have a look at the phases that composes this pipeline.

\subsubsection{Name Entity Recognition}
At first we have a \textbf{Name Entity Recognition} phase, where the data unit (be it a passage or a question) is scoured for expressions or name of monuments that we would like to keep intact in the next phase, where we are going to tokenize the data unit. To keep those expression we modify them in order that the tokenizer sees them as a single word. Example given, the expression "Colonna di Sant'Oronzo" will be recorded, after this passage, as "\textit{colonna\_di\_sant\_oronzo}".
\subsubsection{Tokenization}
The subsequent \textbf{Tokenization} phase is attained trough a Regex. We simply split the unit text trough spaces and punctuation marks. Actually the metacharacter \\W, used in our regex, is intended for every character that is not a word character. A word character is defined as a character from a-z, A-Z, 0-9, including the underscore character. The last bit is very important: not splitting on the underscore characters keeps the Name Entity previously recognized intact.

\begin{python}
re.split('\W', passage.lowercase())
\end{python}


\subsubsection{Elimination of Stop Words}
In the third phase we \textbf{eliminate stop words} from the text. A stop word is defined as a word that has little or no value at all in natural language processing algorithms, mainly because it is too common to be used as a discriminant, like short function words as \textit{and}, \textit{or}, \textit{the}, or \textit{that}. Such words are grouped into a file and those tokens are left out from our data unit. It is important to notice that, in absence of the \textbf{Name Entity Recognition} phase some of this function words would be left out even if they are actually carrying some value, like in "di" from "Colonna di Sant'Oronzo".
\subsubsection{Lemmatization}
The fourth phase, the \textbf{Lemmatization}, brings every word in its base form. Nouns and adjectives are declined to the singular male form, and verbs are brought in infinitive tense. This phase reduces the words that will be used later during the creation of our \textbf{Vector Space Model}, condensing the values of words like "andai", "andammo" and "andrò" in the singular feature "andare", thus simplifying the math and unloading some burden from the processor in the main phase.
\subsubsection{Synonyms}
Finally we are going to reduce even more the number of words used, \textbf{changing synonyms to a known base form}. Until now if two different passages would have had two verbs with the same meaning, even though they are now in their base form, they would have still been two different words, thus two different features in our Vector Space Model. The algorithm would have treated them as two different words, when the meaning is still the same, and as we have seen this is one of the worst problem in NLP. At this point we have considered a list of words that appear many times in our test unit and declined their possible synonyms. This part of the algorithm simply changes, when encountered, a synonym to the "base word" of our choosing.
\subsubsection{An example of preprocessing}
Let us have a look to a brief example, with the phrase \textit{La colonna di Sant'Oronzo si trova in Piazza Sant'Oronzo! Lo sapevi?}. In the first phase we are going to tokenize the phrase, while keeping the Named Entities intact. The result will be a list of words or group of words: [\textit{la, colonna\_di\_sant\_oronzo, si, trova, in, piazza\_sant\_oronzo, lo, sapevi}]. Then we are going to get rid of the stop words \textbf{la, si, in, lo}, getting the following list: [\textit{colonna\_di\_sant\_oronzo, trova, piazza\_sant\_oronzo, sapevi}]. Finally we are going to lemmatize the passage, getting as a result the actual list of features that are going to represent this passage in our Vector Space Model: [\textit{colonna\_di\_sant\_oronzo, trovare, piazza\_sant\_oronzo, sapere}].\\

The entire pipeline is condensed, trough the use of different classes, in a single line of code:
\begin{python}
work_on = synonimer.substituteSynonims(lemmatizer.lemmWords([word for word in re.split('\W', nameEntityRecognizer.recognize(passage)) if word not in stop_words]))
\end{python}

We can have a look at the preprocessing line in Fig. \ref{pipeline}.
\begin{figure}[ht]
\centering
\includegraphics[scale=0.5]{Immagini/pipeline.png}
\caption{\textit{Python Server Pipeline}}
\label{pipeline}
\end{figure}

\subsection{The Space Vector Model}
At this point every data unit is composed of a list of words (or group of words) in their base form, and every list can be composed of a different number of these words or group of words. The idea behind the Vector Space Model is to represent every data unit as a vector in an n-dimensional space, where n is the number of all the possible features in our dataset (in our case the words or group of words survived at the preceding phases), taken once. We are going to create a so-called \textbf{bag of words}, that contains every survived feature and then transform every data unit into an array based on this bag of words trough the mean of simply counting the \textbf{term frequency} of every single term in the data unit.\\
As an example, let us consider a simple bag of words, [\textit{chiesa di santa maria della grazia, colonna di sant'oronzo, deteriorare, piazza sant'oronzo, salsedine, sapere, trovare}] and the data unit analysed in the previous example. It is simple to understand that the Vector Space representation of our data unit will be [\textit{0, 1, 0, 1, 0, 1, 1}].\\
Moreover, it is pretty clear that a very common term among the data units (aside from the stop words, that have been dealt with) is not going to be a good discriminatory term among the documents. Let us suppose, for a moment, that every data unit has at least one occurrence of the term "\textit{monumento}". At this point, the information that the data unit we are searching for has one occurrence of this term is not going to help us discriminating.\\
Basically, term frequency alone would incorrectly emphasize documents which happens to have a lot of occurrences of common words, thus diminishing the weight posed by more specific words, that may be more helpful in distinguishing relevant and non-relevant documents and terms. Hence we incorporate an inverse document frequency factor in the weights of the features in the representation. This way, very common terms among the data units will be weighted down.
\paragraph{Some math:} Let us define the \textbf{term frequency} $tf(t,d)$ as the raw count of the generic term \emph{t} in the generic document \emph{d}, i.e. the number of times that term \emph{t} occurs in document \emph{d}. The inverse document frequency will be 
\begin{equation}
 idf(t,D) = \log \frac{N}{\lvert 1 + \{ d \in D : t \in D\}\rvert }
\end{equation}
where \emph{D} is our corpus, the set of our documents, \emph{N} is the cardinality of the set \emph{D}
\begin{equation}
N = \lvert D \rvert 
\end{equation}
and $ \{ d \in D : t \in D\} $ is the cardinality of the subset of D where $ tf(t,d) \neq 0 $. We add $1$ to this cardinality to avoid any problem related to a division by zero that could happen if a term used in a question is never seen in our dataset, thus leading to a cardinality of $0$. Basically we pretend to have seen  in our dataset each and every term, even those that are not actually in it, once more than we actually did. At this point we can combine the two metrics in the so called \textbf{Term frequency - Inverse document frequency metric}:
\begin{equation}
 tfidf(t,d,D) = tf(t,d) \cdot idf(t,D)
\end{equation}
that is the actual metrics used to identify the values of the elements of the vectors representing our data units in the Vector Space Model.
\subsubsection{Answering a Question}
At this point every data unit (passages of our dataset and the current question) is represented as a vector in our n-dimensional space. Given a question we would like to find the more relevant passage that could answer it. The space vector model assumes, based on the document similarity theories, that a question and the related answer are going to be similar in representation, thus is important to be able to understand when two phrases are similar, so that we can answer a question with the answer more similar to the question itself.
\paragraph{Some math:} As we said before, the Space Vector Model turns every phrase in a vector in our space, defined trough the corpus. At this point, the problem of finding the similarity of two phrases can be approached in a mathematical way, calculating the vector "nearer" to the question's vector. by. An easy way to do this is to compare the angles between each passage vector $\vec p$ and the question vector $\vec q$ is trough the inner product:
\begin{equation}
\cos \theta = \frac{\sum_{i=1}^n (p_i\cdot q_i)}{\sqrt{\sum_{i=1}^n p_i^2} \cdot \sqrt{\sum_{i=1}^n q_i^2} }
\end{equation}


\subsection{Need for further refinements}
The first tests conducted on this primitive form of the algorithm brought to light the fact that it seems inappropriate to the study case. It relies heavily on the lexical correctness of the question posed from the user and, even in presence of a perfectly fine grammar in the test dataset the accuracy of the algorithm was too low (around 34\%) to even consider it as a good starting point. We decided to try and address both the problems at once, changing the approach. The new algorithm resulted limited, but its precision skyrocketed. We are going to analyse it in the next section.

\section{\sc The Python Server: second attempt}
We have redesigned our knowledge base in a more complex way, analysing it to find the more common type of question it can answer. Those turned out to be classifiable in seven ways:
\begin{itemize}
\item \textbf{Definition}: a definition of the monument or part of it, answering questions like "What's this?";
\item \textbf{When}: an answer to simple questions regarding the the year of creation of the monument or piece of art;
\item \textbf{Creator}: an answer to questions about the creator (architect, painter, sculptor) of the monument or piece of art;
\item \textbf{Commissioner}: similar to those tagged with \textbf{Creator}, these text pieces will answer to question regarding the commissioner of a building or piece of art;
\item \textbf{Style}: these text pieces will answer questions regarding the style in which the monument was built;
\item \textbf{Renovation}: this class groups the answers to questions regarding the renovation of the building or piece of art;
\item \textbf{Measurement}: finally, this class will group answers to questions regarding the dimensions of monuments or piece of arts. 
\end{itemize}
Here it is a brief example of our restructured knowledge base:
\begin{lstlisting}[language=json,firstnumber=1]
{
  "ID" : 0,
  "Nomi":[
	"Chiesa di Santa Maria della Grazia"
  ],
  "Position":["40.352420","18.173369"],
  "Def":"La chiesa di Santa Maria della Grazia [...].",
  "When":"Venne edificata negli ultimi decenni del '500.",
  "Style":"Fu costruita nello stile del barocco leccese.",
  "Creator":"L'architetto fu [...].",
  "Children":[
  {
	"ID" : 101,
	"Nomi": [
      "Crocifisso altare lato destro"
	],
	"Position":["40.352420","18.173369"],
	"Def": "Si tratta di un pregevole crocifisso ligneo [...].",
	"Creator": "Fu intagliato da Vespasiano Genuino da Gallipoli"
  }
  ]
}
\end{lstlisting}
It is clear that not every monument will have answers about every kind of question available, mainly since collecting them is not the main concern of this thesis. Another problem, that regards only buildings with other pieces of art inside, is that our information could be relative to the building itself or to the piece of art inside. E.g., the Church of Santa Maria della Grazia contains both some paintings and a fresco of some importance. We addressed the problem structuring the data in a sort of tree data structure. This way we keep information about every item, be it a building or a piece of art inside that building, in the same way, while maintaining information about the physical position of each altogether.\\
At this point, knowing the position and orientation of the user (thus knowing the monument or piece of art his question concerns), we miss just one piece of information to give the right answer. We need to classify the question in one of the seven classes above and return the relative answer. How this will be achieved, the testing about it and the obtained results will be addressed in the proper chapter. 

\section{\sc Bring the server on line: CherryPy}
To allow our Python code to work as a server, and thus to being able to receive questions and answer, we need to bring it online in some way. An easy way to do that is to use CherryPy \cite{cherrypy}, a minimalistic Python framework that allows to create a server program in a fast, smooth way. From the documentation we can see the typical form of a CherryPy application:
\begin{python}
import cherrypy 

class HelloWorld(object):
    def index(self):
        return "Hello World!"
    index.exposed = True

cherrypy.quickstart(HelloWorld())
\end{python} 
And this code is now ready to launch a server with an index page that greets the user with the oldest cliché in informatics.
\subsection{Interacting with the server: GET and POST}
It is very easy with CherryPy to create a tree of methods accessible from the outside client trough the right URL. That said, it is even more interesting to use the actual requests' HTTP methods, like \textbf{GET}, \textbf{POST}, \textbf{PUT}, \textbf{DELETE} and \textbf{OPTIONS} as APIs. It is possible to do so creating methods that are named after them. At this point we need to change the default matching URLs method to one aware of the HTTP requests, \textbf{MethodDispatcher}, already included in the CherryPy's package.

\begin{python}
import cherrypy
import random

@cherrypy.expose
class StringGeneratorWebService(object):
  @cherrypy.tools.accept(media='text/plain')
  def GET(self):
    return cherrypy.session['mystring']

  def POST(self, length=8):
    some_string = ''.join(random.sample(string.hexdigits, int(length)))
    return some_string

if __name__ == '__main__':
  conf = {
    '/': {
      'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
      'tools.response_headers.on': True,
      'tools.response_headers.headers': [('Content-Type', 'text/plain')],
    }
  }
cherrypy.quickstart(StringGeneratorWebService(), '/', conf)
\end{python}

\subsection{Handling CORS}
As we explained before, writing an application with the Ionic framework is an easy way to get rid of most of the problem related to having three different OS to work with. This convenience comes at a price though, and here we will handle part of that price. A mobile application written in Ionic will use the native browser methods to connect to an external server and, for security reasons, browsers restrict cross-origin HTTP requests initiated from within scripts. 
\paragraph{A cross-origin HTTP request}is defined as the requests for a resource that has a different origin (domain, protocol, and port) than its own origin. \\
The CORS (Cross Origin Resource Sharing) standard mechanism supports secure cross-origin requests and data transfers between browsers and web servers. It basically adds new HTTP headers that allow servers to describe the set of origins that are permitted to read that information using a web browser; it is necessary to add these headers in the server configuration.\\
Additionally, for HTTP request methods that can cause side-effects on server's data (basically anyone other than the GET), the specification mandates that browsers "preflight" the request, soliciting supported methods from the server with an HTTP OPTIONS request method, and then, upon "approval" from the server, sending the actual request with the actual HTTP request method. \cite{CORS}\\
It is easy to understand then, that these necessities needs to be addressed in the developing of the server. We can solve the problem easily trough the use of a method external to the class that we are bringing online that is going to operate a minimal configuration:

\begin{python}
def CORS():
    """Allow web apps not on the same server to use our API
    """
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*" 
    cherrypy.response.headers["Access-Control-Allow-Headers"] = (
        "content-type, Authorization, X-Requested-With"
    )
    
    cherrypy.response.headers["Access-Control-Allow-Methods"] = (
        'GET, POST, PUT, DELETE, OPTIONS'
    )
    
if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS);
\end{python}

\section{\sc The Mobile Application}
The mobile application have been written trough the Ionic framework.
\paragraph{Ionic}is a complete open-source SDK for hybrid mobile application development created by Max Lynch, Ben Sperry and Adam Bradley of Drifty Co. in 2013. The original version was released in 2013 and built on top of AngularJS and Apache Cordova. The more recent releases, known as Ionic 3, are built on Angular4. Ionic provides tools and services for developing hybrid mobile applications using Web technologies like CSS, HTML5, and Sass and works with Cordova plugins to gain access to host operating systems features such as camera and GPS. This means that trough proper configuration of the Cordova plugins we can write the application once, almost as we were programming a web application, and deploy for every mobile operating system, from Android to iOS or even Windows Phone (even if it is discontinued as a OS).\\
This meant less effort in the production of the mobile application, allowing us to focus more on the other parts of this work.
\subsection{User Interface}
It is been easy, thanks to Ionic, to put up a polished user interface for the mobile application. The interesting part is that, if we decide to deploy the same application, the user interface would change without any intervention on our part, according to the target OS.\\
Let us have a look at the user interface. In Fig. \ref{fig:interface1} and Fig. \ref{fig:interface2} we can see how the application looks in "exploration mode", when we are just strolling in Sant'Oronzo' Square and approaching the various monuments. Once we have found a monument we are interested in, we can start typing our question, and a tappable icon will appear, allowing us to send the question to the server, as we can see in \ref{fig:interface3}. Finally, the application keeps track of all our questions and the answer that were given in a separate tab, as we can see in \ref{fig:interface4}. We can easily scroll up and down our previous questions and give a second look even when we are back home, away from Sant'Oronzo' Square.

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.35\textwidth}
    		\centering
        	\includegraphics[width=\textwidth]{Immagini/Interface/interface1.png}
        	\caption[3550]%
        	{{\small \textit{Palazzo Carafa}}}
        	\label{fig:interface1}  
    	\end{subfigure}
    	\hfill
    	\begin{subfigure}[b]{0.35\textwidth}  
        	\centering 
        	\includegraphics[width=\textwidth]{Immagini/Interface/interface4.png}
        	\caption[]%
        	{{\small \textit{Santa Maria della Grazia}}}
       	 	\label{fig:interface2}    
    	\end{subfigure}
        \vskip\baselineskip
        \centering
		\begin{subfigure}[b]{0.35\textwidth}
    		\centering
        	\includegraphics[width=\textwidth]{Immagini/Interface/interface3.png}
        	\caption[3550]%
        	{{\small \textit{Answering a question}}}
        	\label{fig:interface3}  
    	\end{subfigure}
    	\hfill
    	\begin{subfigure}[b]{0.35\textwidth}  
        	\centering 
        	\includegraphics[width=\textwidth]{Immagini/Interface/interface2.png}
        	\caption[]%
        	{{\small \textit{Asked questions' History}}}
        	\label{fig:interface4}    
    	\end{subfigure}
        \caption{ } 
        \label{fig:interface}
\end{figure}