\select@language {UKenglish}
\contentsline {chapter}{\sc Introduction}{II}{section*.3}
\contentsline {chapter}{\numberline {1}\sc Climate Diagnostic}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\sc Big data challenge}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}\sc Il CESM e l'AMWG diagnostic package}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}\sc Il Community Earth System Model}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}\sc L'AMWG diagnostics package}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Limiti dell'AMWG diagnostics package}{6}{subsection.2.2.1}
