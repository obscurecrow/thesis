\babel@toc {UKenglish}{}
\contentsline {chapter}{\sc Introduction}{III}{section*.3}
\contentsline {paragraph}{Chapter 1}{IV}{section*.4}
\contentsline {paragraph}{Chapter 2}{IV}{section*.5}
\contentsline {paragraph}{Chapter 3}{IV}{section*.6}
\contentsline {paragraph}{Chapter 4}{IV}{section*.7}
\contentsline {paragraph}{Chapter 5}{IV}{section*.8}
\contentsline {paragraph}{Conclusions and future applications}{IV}{section*.9}
\contentsline {chapter}{\numberline {1}\sc Question Answering Systems}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\sc Definition and main features}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Paradigms of question answering}{2}{subsection.1.1.1}
\contentsline {paragraph}{Search Engines}{2}{section*.11}
\contentsline {paragraph}{Question Answering Systems}{2}{section*.12}
\contentsline {paragraph}{Conversational Agents}{2}{section*.13}
\contentsline {section}{\numberline {1.2}\sc Question taxonomy}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}\sc A review of today's applications}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}\sc At the bottom of this: Natural Language Processing}{5}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}NLP issues}{6}{subsection.1.4.1}
\contentsline {chapter}{\numberline {2}\sc Chatbots in real life environments }{8}{chapter.2}
\contentsline {section}{\numberline {2.1}\sc Chatbots and Museums}{8}{section.2.1}
\contentsline {paragraph}{House Museums of Milan,}{8}{section*.14}
\contentsline {paragraph}{The National Art Museum in Belarus}{9}{section*.15}
\contentsline {paragraph}{The Cooper-Hewitt Museum in New York,}{9}{section*.16}
\contentsline {paragraph}{The Anne Frank House in Amsterdam}{9}{section*.17}
\contentsline {paragraph}{The city of Turin}{9}{section*.18}
\contentsline {section}{\numberline {2.2}\sc Citybots: How a chatbot can change a city}{9}{section.2.2}
\contentsline {paragraph}{Joshua Browder,}{10}{section*.19}
\contentsline {paragraph}{The United Kingdom}{10}{section*.20}
\contentsline {paragraph}{Udayan Vidyanta}{10}{section*.21}
\contentsline {chapter}{\numberline {3}\sc Platforms and Technologies}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}\sc Analysis of possible platforms}{11}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Email}{11}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}SMS}{11}{subsection.3.1.2}
\contentsline {subsubsection}{Twilio}{12}{section*.22}
\contentsline {subsection}{\numberline {3.1.3}Whatsapp}{12}{subsection.3.1.3}
\contentsline {subsubsection}{YowSup}{12}{section*.23}
\contentsline {subsection}{\numberline {3.1.4}Facebook Messenger}{12}{subsection.3.1.4}
\contentsline {subsubsection}{Facebook Messenger API}{13}{section*.24}
\contentsline {subsubsection}{The built in Natural Language Processing}{14}{section*.25}
\contentsline {subsubsection}{Cons of Facebook Messenger}{14}{section*.26}
\contentsline {subsection}{\numberline {3.1.5}Telegram}{15}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}App}{15}{subsection.3.1.6}
\contentsline {section}{\numberline {3.2}\sc Useful tools for the implementation of QASs}{15}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Injecting Artificial Intelligence in our Chatbot}{16}{subsection.3.2.1}
\contentsline {subsubsection}{Google Artificial Intelligence API}{16}{section*.27}
\contentsline {subsubsection}{Python Libraries}{16}{section*.28}
\contentsline {chapter}{\numberline {4}\sc Project Analysis}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}\sc Project Requirements}{17}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}The expected user experience}{17}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}\sc The Python Server: first attempt}{18}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Passages and Questions}{18}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}The Preprocessing}{19}{subsection.4.2.2}
\contentsline {subsubsection}{Name Entity Recognition}{19}{section*.29}
\contentsline {subsubsection}{Tokenization}{19}{section*.30}
\contentsline {subsubsection}{Elimination of Stop Words}{20}{section*.31}
\contentsline {subsubsection}{Lemmatization}{20}{section*.32}
\contentsline {subsubsection}{Synonyms}{20}{section*.33}
\contentsline {subsubsection}{An example of preprocessing}{20}{section*.34}
\contentsline {subsection}{\numberline {4.2.3}The Space Vector Model}{21}{subsection.4.2.3}
\contentsline {paragraph}{Some math:}{22}{section*.36}
\contentsline {subsubsection}{Answering a Question}{22}{section*.37}
\contentsline {paragraph}{Some math:}{23}{section*.38}
\contentsline {subsection}{\numberline {4.2.4}Need for further refinements}{23}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}\sc The Python Server: second attempt}{23}{section.4.3}
\contentsline {section}{\numberline {4.4}\sc Bring the server on line: CherryPy}{25}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Interacting with the server: GET and POST}{25}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Handling CORS}{26}{subsection.4.4.2}
\contentsline {paragraph}{A cross-origin HTTP request}{26}{section*.39}
\contentsline {section}{\numberline {4.5}\sc The Mobile Application}{27}{section.4.5}
\contentsline {paragraph}{Ionic}{27}{section*.40}
\contentsline {subsection}{\numberline {4.5.1}User Interface}{27}{subsection.4.5.1}
\contentsline {chapter}{\numberline {5}\sc Question Classification}{30}{chapter.5}
\contentsline {section}{\numberline {5.1}\sc Classification Algorithms}{30}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Naive Bayes Classifier}{30}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Logistic Regression}{31}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Linear Support Vector Classifier}{32}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}\sc Benchmarking Classification Algorithms}{33}{section.5.2}
\contentsline {paragraph}{Accuracy}{33}{section*.44}
\contentsline {paragraph}{Precision}{34}{section*.45}
\contentsline {paragraph}{Recall}{34}{section*.46}
\contentsline {paragraph}{F Measure}{34}{section*.47}
\contentsline {subsection}{\numberline {5.2.1}Training and test data}{34}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Results of the benchmarking}{35}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Performances of the Linear SVC}{36}{subsection.5.2.3}
\contentsline {chapter}{\sc Conclusions}{38}{section*.54}
\contentsline {chapter}{Appendices}{}{section*.57}
\contentsline {chapter}{Appendices}{}{section*.58}
\contentsline {section}{\numberline {.1}Web Server Configuration}{}{section.Alph0.1}
\contentsline {section}{\numberline {.2}Web Server Application}{}{section.Alph0.2}
\contentsline {section}{\numberline {.3}Web Server Classifier}{}{section.Alph0.3}
\contentsline {section}{\numberline {.4}Tagged Questions dataset}{}{section.Alph0.4}
